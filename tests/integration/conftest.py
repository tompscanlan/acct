import mock
import pytest


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="evbus")

    with mock.patch("sys.argv", ["acct"]):
        hub.pop.config.load(["acct"], cli="acct")

    yield hub
