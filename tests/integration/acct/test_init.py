import mock
import pop.hub
import pop.contract
import pytest
import tempfile
import yaml

credentials_yaml = """
backend_key: my-backend-key

default: my-default

provider:
  profile:
    kwarg1: value1
    list1:
      - item1
      - item2
"""


def test_cli(hub):
    """
    Test the encryption and decryption capabilities of the cli tool
    """
    # A temporary file to write encoded data to and read encoded data from
    with tempfile.NamedTemporaryFile(delete=True) as enc_fh:
        # A temporary file to write unencrypted data to
        with tempfile.NamedTemporaryFile(delete=True) as raw_fh:
            raw_fh.write(credentials_yaml.encode())
            raw_fh.flush()
            with mock.patch(
                "sys.argv",
                [
                    "acct",
                    "encrypt",
                    raw_fh.name,
                    "--crypto-plugin=fernet",
                    f"--output-file={enc_fh.name}",
                ],
            ):
                with mock.patch("builtins.print") as printed:
                    hub.acct.init.cli()
                    # Retrieve the key that was printed out
                    acct_key = printed.call_args[0][0]

        # Decrypt a the encrypted file with the key
        with mock.patch(
            "sys.argv",
            [
                "acct",
                "decrypt",
                enc_fh.name,
                f"--acct-key={acct_key}",
                "--crypto-plugin=fernet",
                "--output=yaml",
            ],
        ):
            with mock.patch("builtins.print") as printed:
                hub.acct.init.cli()
                # Retrieve the output of the decryption process
                decrypted_file = printed.call_args[0][0]

    # Compare notes, they should be exactly  the same
    assert yaml.safe_load(decrypted_file) == yaml.safe_load(credentials_yaml)


@pytest.mark.asyncio
async def test_backends(hub):
    hub.acct.backend.provider = pop.hub.Sub(hub, subname="provider")
    hub.acct.backend.provider.unlock = pop.contract.Contracted(
        hub, [], lambda hub, **kwargs: kwargs, ref="", name="lamb"
    )

    provider = {"provider": {"profile": {"kwarg1": "val1"}}}
    profiles = {
        "backend_key": "my_backend_key",
        "default": "my_default",
        "my_backend_key": provider,
    }
    backends = await hub.acct.init.backends(profiles)
    assert backends == provider


@pytest.mark.asyncio
async def test_profiles(hub):
    data = {"foo": "bar", "baz": "bop"}

    with tempfile.NamedTemporaryFile(delete=True) as fh:
        key = hub.crypto.fernet.generate_key()
        enc = hub.crypto.fernet.encrypt(data, key)
        fh.write(enc)
        fh.flush()

        profiles = await hub.acct.init.profiles(
            acct_file=fh.name, acct_key=key, crypto_plugin="fernet"
        )

    assert profiles == data


@pytest.mark.asyncio
async def test_process(hub):
    ...


@pytest.mark.asyncio
async def test_unlock(hub):
    data = {
        "default": "my_default",
        "backend_key": "my_backend_key",
        "foo": {"bar": ["baz"]},
    }

    with tempfile.NamedTemporaryFile(delete=True) as fh:
        key = hub.crypto.fernet.generate_key()
        enc = hub.crypto.fernet.encrypt(data, key)
        fh.write(enc)
        fh.flush()

        await hub.acct.init.unlock(acct_file=fh.name, acct_key=key)
        assert hub.acct.UNLOCKED
        assert hub.acct.DEFAULT == "my_default"
        assert hub.acct.BACKEND_KEY == "my_backend_key"
        assert hub.acct.PROFILES == {"foo": {"bar": ["baz"]}}


@pytest.mark.asyncio
async def test_single(hub):
    await hub.acct.init.single(subs=[], profile_name="", sub_profiles={}, profiles={})


@pytest.mark.asyncio
async def test_gather(hub):
    hub.acct.UNLOCKED = True
    await hub.acct.init.gather([], "")
